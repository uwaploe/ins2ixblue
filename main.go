// Ins2ixblue creates and sends iXBlue external sensor messages to the
// Rovins INS using the GNSS data from the Inertial Labs INS.
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	ins "bitbucket.org/uwaploe/go-ins/v2"
	"bitbucket.org/uwaploe/ixblue"
	"github.com/nats-io/stan.go"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: ins2ixblue [options] host:port

Subscribe to the INS data and generate ixBlue external sensor messages to
send to the Rovins INS at HOST:PORT.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false,
		"Log debugging output")
	natsURL    string = "nats://localhost:4222"
	clusterID  string = "must-cluster"
	insSubject string = "insv2.data"
	protocol   string = "tcp"
	forceAlt   bool
	posStddev  float64 = 3
)

// Value of GNSSInfo[1] which indicates a valid fix
const ValidGnss uint8 = 0x1c

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&insSubject, "sub", lookupEnvOrString("INS_SUBJECT", insSubject),
		"Subject name for INS data")
	flag.StringVar(&protocol, "proto", protocol, "Network protocol for Rovins input")
	flag.BoolVar(&forceAlt, "force-alt", forceAlt, "Force altitude value to be zero")
	flag.Float64Var(&posStddev, "std",
		lookupEnvOrFloat("GPS_STDDEV", posStddev),
		"Standard deviation of GPS position (m)")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	rovinsAddr := args[0]

	var (
		status int
	)
	defer func() { os.Exit(status) }()

	sc, err := stan.Connect(clusterID, "rovins-gnss", stan.NatsURL(natsURL))
	if err != nil {
		log.Printf("Cannot connect: %v", err)
		status = 1
		return
	}
	defer sc.Close()

	conn, err := net.Dial(protocol, rovinsAddr)
	if err != nil {
		log.Print("Cannot access Rovins")
		status = 1
		return
	}

	if tcpConn, ok := conn.(*net.TCPConn); ok {
		tcpConn.SetWriteBuffer(128)
	}

	ch := make(chan ixblue.SensorBlock, 1)

	insCb := func(m *stan.Msg) {
		var rec ins.DataRecord
		err := msgpack.Unmarshal(m.Data, &rec)
		if err != nil {
			log.Printf("Msgpack decode error: %v", err)
			return
		}

		if (rec.GnssInfo[1] & ValidGnss) == ValidGnss {
			t := rec.Timestamp()
			block := ixblue.Gnss1Data{
				GnssData: ixblue.GnssData{
					Tvalid:    ixblue.Ticks(t),
					Id:        0,
					Qual:      ixblue.QualDiff,
					Lat:       rec.GnssLat,
					Alt:       float32(rec.GnssAlt),
					LatStddev: rec.GnssLatStd,
					LonStddev: rec.GnssLonStd,
				},
			}

			// iXBlue uses East longitude; 0-360
			if rec.GnssLon < 0 {
				block.Lon = rec.GnssLon + 360
			} else {
				block.Lon = rec.GnssLon
			}

			if forceAlt {
				block.Alt = 0
			}

			ch <- &block
		}
	}

	sub, err := sc.Subscribe(insSubject, insCb)
	if err != nil {
		log.Fatalf("Cannot subscribe to INS data: %v", err)
	}
	defer sub.Unsubscribe()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("Rovins GNSS service starting %s", Version)

	enc := ixblue.NewEncoder(conn)
	for {
		select {
		case sig := <-sigs:
			log.Printf("Exiting on %v signal", sig)
			return
		case block := <-ch:
			conn.SetWriteDeadline(time.Now().Add(time.Millisecond * 500))
			if err := enc.Encode(block); err != nil {
				log.Printf("Error sending GNSS data: %v", err)
				return
			}
		}
	}
}
